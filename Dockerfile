FROM node:latest

COPY entrypoint.sh /
COPY laravel-echo-server.json /
RUN \
	npm install -g laravel-echo-server && \
	chmod +x entrypoint.sh

ENV \
	ECHO_AUTH_HOST=http://localhost \
	ECHO_DEBUG=false \
	ECHO_SSL_CERT_PATH= \
	ECHO_SSL_KEY_PATH= \
	ECHO_SSL_CHAIN_PATH= \
	ECHO_SSL_PASSPHRASE= \
	ECHO_PROTOCOL=http \
	ECHO_REDIS_PORT=6379 \
	ECHO_REDIS_HOSTNAME=redis \
	ECHO_REDIS_PASSQORD= \
	ECHO_DEVMODE=false \
	ECHO_CLIENTS=[] \
	ECHO_ALLOW_CORS=true \
	ECHO_ALLOW_ORIGIN=http://localhost:80 \
	ECHO_ALLOW_METHODS="GET, POST" \
	ECHO_ALLOW_HEADERS="Origin, Content-Type, X-Auth-Token, X-Requested-With, Accept, Authorization, X-CSRF-TOKEN, X-Socket-Id"


WORKDIR /


ENTRYPOINT ["/entrypoint.sh"]
CMD ["laravel-echo-server", "start", "--dir=/"]
