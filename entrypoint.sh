#!/bin/sh

set -e

sed -i -e "s%{{ ECHO_AUTH_HOST }}%$ECHO_AUTH_HOST%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_DEBUG }}%$ECHO_DEBUG%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_CLIENTS }}%$ECHO_CLIENTS%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_REDIS_PORT }}%$ECHO_REDIS_PORT%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_REDIS_HOSTNAME }}%$ECHO_REDIS_HOSTNAME%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_REDIS_PASSWORD }}%$ECHO_REDIS_PASSWORD%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_DEVMODE }}%$ECHO_DEVMODE%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_PROTOCOL }}%$ECHO_PROTOCOL%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_SSL_CERT_PATH }}%$ECHO_SSL_CERT_PATH%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_SSL_KEY_PATH }}%$ECHO_SSL_KEY_PATH%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_SSL_CHAIN_PATH }}%$ECHO_SSL_CHAIN_PATH%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_SSL_PASSPHRASE }}%$ECHO_SSL_PASSPHRASE%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_ALLOW_CORS }}%$ECHO_ALLOW_CORS%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_ALLOW_ORIGIN }}%$ECHO_ALLOW_ORIGIN%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_ALLOW_METHODS }}%$ECHO_ALLOW_METHODS%g" /laravel-echo-server.json
sed -i -e "s%{{ ECHO_ALLOW_HEADERS }}%$ECHO_ALLOW_HEADERS%g" /laravel-echo-server.json

$@


cat /laravel-echo-server.json
